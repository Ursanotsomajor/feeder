//
//  VKHelper.m
//  Feeder
//
//  Created by Edward Lisin on 27/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "VKHelper.h"

@implementation VKHelper

+ (void)getFeedWithOffset:(NSString *)startFrom Completion:(void (^)(NSDictionary *, VKHelperError error))completion
{
    if (![[VKSdk accessToken] accessToken])
    {
        completion(nil, VKHelperTokenError);
        return;
    }
    
    VKRequest *getWall = [VKRequest requestWithMethod:@"newsfeed.get"
                                           parameters:@{@"count": @"100", @"return_banned": @"0", @"filters": @"post",
                                                        @"start_from": startFrom,
                                                        @"access_token": [[VKSdk accessToken] accessToken]}];
    
    [getWall executeWithResultBlock:^(VKResponse *response) {
        
        if (response.json)
        {
            completion(response.json, 0);
        }
        
    } errorBlock:^(NSError *error) {
        
        if ([error.localizedDescription containsString:@"offline"])
        {
            completion(nil, VKHelperConnectionError);
            return;
        }
        
        completion(nil, VKHelperTokenUnknownError);
        
    }];
}

+ (void)getUserInfoForID:(NSString *)userID completion:(void (^)(NSDictionary *))completion
{
    VKRequest *getUser = [VKRequest requestWithMethod:@"users.get" parameters:@{@"user_ids": userID,
                                                                                @"fields": @"photo_100"}];
    
    [getUser executeWithResultBlock:^(VKResponse *response) {
        
        if (response.json[0])
        {
            completion(response.json[0]);
        }
        
    } errorBlock:^(NSError *error) {
        
        completion(nil);
        
    }];
}

+ (void) getGroupInfoForID:(NSString *)groupID completion:(void (^)(NSDictionary *groupItem))completion
{
    VKRequest *getGroups = [VKRequest requestWithMethod:@"groups.getById" parameters:@{@"group_ids": groupID,
                                                                                       @"fields": @"name"}];
    
    [getGroups executeWithResultBlock:^(VKResponse *response) {
        
        if (response.json[0])
        {
            completion(response.json[0]);
        }
        
    } errorBlock:^(NSError *error) {
        
        completion(nil);
        
    }];
}

+ (void)logout
{
    [VKSdk forceLogout];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Username"];
}

@end
