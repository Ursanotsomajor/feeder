//
//  FeedTableViewCell.m
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "FeedTableViewCell.h"


@interface FeedTableViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *repostHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postTextViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *repostTextViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelHeight;

@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *postTextView;

@property (weak, nonatomic) IBOutlet UILabel *repostNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *repostTextView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end


@implementation FeedTableViewCell


- (void)setCacheItem:(CacheItem *)cacheItem
{
    _cacheItem = cacheItem;
    
    [Expansion runOnMainThread:^{
        
        _senderNameLabel.text = cacheItem.senderName;
        _dateLabel.text = cacheItem.date;
        
        _postTextView.text = (cacheItem.senderText && cacheItem.senderText.length > 0) ? cacheItem.senderText : nil;
        _postTextViewHeight.constant = (cacheItem.senderText && cacheItem.senderText.length > 0) ? [self textViewHeightForText:_cacheItem.senderText] : 0;
        
        if (cacheItem.isRepost)
        {
            _repostHeaderHeight.constant = 60;
            _repostNameLabel.text = cacheItem.repostName;
            
            _repostTextView.text = (cacheItem.repostText && cacheItem.repostText.length > 0) ? cacheItem.repostText : nil;
            _repostTextViewHeight.constant = (cacheItem.repostText && cacheItem.repostText.length > 0) ? [self textViewHeightForText:_cacheItem.repostText] : 0;
            
            _infoLabel.text = (cacheItem.senderPhotos.count > 1 || cacheItem.repostPhotos.count > 1) ? @"Внутри еще фотографии" : @"";
            _infoLabelHeight.constant = (cacheItem.senderPhotos.count > 1 || cacheItem.repostPhotos.count > 1) ? 30 : 0;
        }
        else
        {
            _repostHeaderHeight.constant = 0;
            _repostAvatarImageView.image = nil;
            _repostNameLabel.text = nil;
            _repostTextView.text = nil;
            _repostTextViewHeight.constant = 0;
            
            _infoLabel.text = (cacheItem.senderPhotos.count > 1) ? @"Внутри еще фотографии" : @"";
            _infoLabelHeight.constant = (cacheItem.senderPhotos.count > 1) ? 30 : 0;
        }
        
    }];
}

- (CGFloat) textViewHeightForText:(NSString *)text
{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(_postTextView.width, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName: _postTextView.font}
                                                    context:nil];
    
    return rect.size.height + 20;
}


- (void)setPostImage:(UIImage *)postImage
{
    [Expansion runOnMainThread:^{
        
        _postImage = postImage;
        _postImageView.image = _postImage;
        
        _postImageView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            _postImageView.alpha = 1;
            
        }];

    }];
}

- (void)setRepostImage:(UIImage *)repostImage
{
    [Expansion runOnMainThread:^{
        
        _repostImage = repostImage;
        _repostImageView.image = repostImage;
        
        _repostImageView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            _repostImageView.alpha = 1;
            
        }];

    }];
}

- (void)setAvatarImage:(UIImage *)avatarImage
{
    [Expansion runOnMainThread:^{
        
        _avatarImage = avatarImage;
        _avatarImageView.image = _avatarImage;
        
        _avatarImageView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            _avatarImageView.alpha = 1;
            
        }];
        
    }];
}

- (void)setRepostAvatarImage:(UIImage *)repostAvatarImage
{
    [Expansion runOnMainThread:^{
        
        _repostAvatarImage = repostAvatarImage;
        _repostAvatarImageView.image = _repostAvatarImage;
        
        _repostAvatarImageView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            _repostAvatarImageView.alpha = 1;
            
        }];
        
    }];
}

@end
