//
//  DataSource.h
//  Feeder
//
//  Created by Edward Lisin on 03/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Expansion.h"
#import "VKHelper.h"
#import "CoreDataController.h"
#import "CacheItem.h"
#import "PhotoItem.h"


@interface DataSource : NSObject

@property CoreDataController *coreDataController;


- (void) getImageForLink:(NSString *)link withCompletion:(void (^)(UIImage *))completion;

- (void) cacheItemWithDictionary:(NSDictionary *)dictionary completion:(void (^)(CacheItem *cacheItem))completion;
- (CGFloat) heightForCacheItemWithDictionary:(NSDictionary *)dictionary width:(CGFloat)width;

@end
