//
//  CachedItem.h
//  Feeder
//
//  Created by Edward Lisin on 17/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CachedItem : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CachedItem+CoreDataProperties.h"
