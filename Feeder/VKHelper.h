//
//  VKHelper.h
//  Feeder
//
//  Created by Edward Lisin on 27/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>
#import "Expansion.h"

#define VK_APP_ID @"5417146"
#define VK_SECURE_KEY @"kQ93OrnlHNUUped7YUJ8"


typedef enum : NSInteger{
    VKHelperTokenError = 1,
    VKHelperConnectionError = 2,
    VKHelperTokenUnknownError = 3
} VKHelperError;


@interface VKHelper : NSObject

+ (void) getFeedWithOffset:(NSString *)startFrom
                Completion:(void (^)(NSDictionary *feedItem, VKHelperError error))completion;

+ (void) getUserInfoForID:(NSString *)userID completion:(void (^)(NSDictionary *userItem))completion;
+ (void) getGroupInfoForID:(NSString *)groupID completion:(void (^)(NSDictionary *groupItem))completion;

+ (void) logout;
@end
