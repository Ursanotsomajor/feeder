//
//  CoreDataController.h
//  Feeder
//
//  Created by Edward Lisin on 13/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Expansion.h"
#import "Photo.h"
#import "CachedItem.h"
#import "CacheItem.h"

@interface CoreDataController : NSObject

//Photo
- (void) insertPhotoWithLink:(NSString *)link imageData:(NSData *)imageData;
- (NSData *) imageDataForLink:(NSString *)link;
- (BOOL) hasImageForLink:(NSString *)link;
+ (NSString *) shortAdressFromLink:(NSString *)link;


//CachedItem
- (void) insertCacheItem:(CacheItem *)item withID:(NSString *)itemID;
- (CacheItem *) cachedItemForID:(NSString *)itemID;
- (BOOL) hasCachedItemForID:(NSString *)itemID;


//Other
- (void) removeAllEntities;

@end
