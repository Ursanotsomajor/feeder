//
//  CacheItem.m
//  Feeder
//
//  Created by Edward Lisin on 02/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "CacheItem.h"

@implementation CacheItem

- (NSArray *)arrayWithItems
{
    NSMutableArray *arr = [NSMutableArray new];
    
    
    [arr addObject:@{@"Name": self.senderName, @"Date": self.date, @"Avatar": self.senderAvatarLink}];
    
    
    if (self.senderText && self.senderText.length > 0)
    {
        [arr addObject:self.senderText];
    }
    
    
    if (self.senderPhotos.count)
    {
        for (id photo in self.senderPhotos)
        {
            [arr addObject:photo];
        }
    }
    
    
    if (self.isRepost)
    {
        [arr addObject:@{@"Name": self.repostName, @"Avatar": self.repostAvatarLink}];
        
        if (self.repostText && self.repostText.length > 0)
        {
            [arr addObject:self.repostText];
        }
        
        if (self.repostPhotos.count)
        {
            for (id photo in self.repostPhotos)
            {
                [arr addObject:photo];
            }
        }
    }
    
    return [arr copy];
}

@end
