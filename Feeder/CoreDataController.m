//
//  CoreDataController.m
//  Feeder
//
//  Created by Edward Lisin on 13/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "CoreDataController.h"

#define REMOVING_TIME_INTERVAL_IN_SECONDS 60 * 60 * 12 //12 hours

@interface CoreDataController()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end


@implementation CoreDataController

- (id)init
{
    self = [super init];
    
    if (!self)
        return nil;
    
    [self initializeCoreData];
    [self removeRedundantEntities];
    
    return self;
}

- (void)initializeCoreData
{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(mom != nil, @"Error initializing Managed Object Model");
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    
    [moc setPersistentStoreCoordinator:psc];
    [self setManagedObjectContext:moc];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [documentsURL URLByAppendingPathComponent:@"Model.sqlite"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        NSError *error = nil;
        NSPersistentStoreCoordinator *psc = [[self managedObjectContext] persistentStoreCoordinator];
        NSDictionary *persistentOptions = @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES};
        NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                               URL:storeURL options:persistentOptions error:&error];
        NSAssert(store != nil, @"Error initializing PSC: %@\n%@", [error localizedDescription], [error userInfo]);
    });
}

+ (NSString *)shortAdressFromLink:(NSString *)link
{
    return [link substringFromIndex:[[NSString stringWithFormat:@"https://pp.vk.me/"] length]];
}

#pragma mark – Photo
- (void) insertPhotoWithLink:(NSString *)link imageData:(NSData *)imageData
{
    Photo *newPhoto = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:_managedObjectContext];
    newPhoto.imageData = imageData;
    newPhoto.url = [CoreDataController shortAdressFromLink:link];
    newPhoto.createdAt = [NSDate date];
    
    NSError *error = nil;
    if (![_managedObjectContext save:&error])
    {
        [Expansion debugMessage:@"Saving photo unsuccessfull" withError:error forClass:self methodName:@"insertPhotoWithLink"];
    }
}

- (NSData *)imageDataForLink:(NSString *)link
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"url == %@", link];
    
    NSError  *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (items.count > 0)
    {
        for(Photo *photo in items)
        {
            if([photo.url isEqualToString:link])
            {
                return photo.imageData;
            }
        }
    }
    
    return nil;
}

- (BOOL) hasImageForLink:(NSString *)link
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"url == %@", link];
    
    NSError *error = nil;
    NSUInteger count = [_managedObjectContext countForFetchRequest:request error:&error];
    
    if (count == NSNotFound || count < 1)
    {
        return 0;
    }
    
    return 1;
}



#pragma mark – Cache Item
- (void)insertCacheItem:(CacheItem *)item withID:(NSString *)itemID
{
    CachedItem *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"CachedItem" inManagedObjectContext:_managedObjectContext];
    newItem.date = item.date;
    newItem.isRepost = item.isRepost ? @1: @0;
    newItem.postID = [item.postID stringValue];
    
    newItem.repostAvatarLink = item.repostAvatarLink;
    newItem.repostName = item.repostName;
    newItem.repostText = item.repostText;
    newItem.repostPhotos = [NSKeyedArchiver archivedDataWithRootObject:item.repostPhotos];
    
    newItem.senderAvatarLink = item.repostAvatarLink;
    newItem.senderName = item.senderName;
    newItem.senderText = item.senderText;
    newItem.senderPhotos = [NSKeyedArchiver archivedDataWithRootObject:item.senderPhotos];
    
    NSError *error = nil;
    if (![_managedObjectContext save:&error])
    {
        [Expansion debugMessage:@"Saving cacheItem unsuccessfull" withError:error forClass:self methodName:@"insertCacheItem"];
    }
}

- (CacheItem *)cachedItemForID:(NSString *)itemID
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CachedItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"postID == %@", itemID];
    
    NSError  *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (items.count > 0)
    {
        for (CachedItem *item in items)
        {
            if ([item.postID isEqualToString:itemID])
            {
                CacheItem *cacheItem = [[CacheItem alloc] init];
                
                cacheItem.date = item.date;
                cacheItem.isRepost = item.isRepost;
                cacheItem.postID = item.postID;
                
                cacheItem.repostAvatarLink = item.repostAvatarLink;
                cacheItem.repostName = item.repostName;
                cacheItem.repostText = item.repostText;
                cacheItem.repostPhotos = [NSKeyedUnarchiver unarchiveObjectWithData:item.repostPhotos];
                
                cacheItem.senderAvatarLink = item.senderAvatarLink;
                cacheItem.senderName = item.senderName;
                cacheItem.senderText = item.senderText;
                cacheItem.senderPhotos = [NSKeyedUnarchiver unarchiveObjectWithData:item.senderPhotos];
                
                return cacheItem;
            }
        }
    }
    
    return nil;
}

- (BOOL)hasCachedItemForID:(NSString *)itemID
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"CachedItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"postID == %@", itemID];
    
    NSError *error = nil;
    NSUInteger count = [_managedObjectContext countForFetchRequest:request error:&error];
    
    if (count == NSNotFound || count < 1)
    {
        return 0;
    }
    
    return 1;
}


#pragma mark – Other

- (void)removeAllEntities
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    if (![_managedObjectContext save:&error])
    {
        [Expansion debugMessage:@"Delete unsuccessfull" withError:error forClass:self methodName:@"removeAllEntities"];
    }
}


- (void) removeRedundantEntities
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *photos = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (Photo *photo in photos)
    {
        NSDate *now = [NSDate date];
        NSDate *createdAt = photo.createdAt;
        
        if (fabs([createdAt timeIntervalSinceDate:now]) > REMOVING_TIME_INTERVAL_IN_SECONDS)
        {
            [_managedObjectContext deleteObject:photo];
        }
    }
    
    if (![_managedObjectContext save:&error])
    {
        [Expansion debugMessage:@"Delete unsuccessfull" withError:error forClass:self methodName:@"removeAllEntities"];
    }
}

@end
