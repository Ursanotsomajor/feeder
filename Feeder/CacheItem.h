//
//  CacheItem.h
//  Feeder
//
//  Created by Edward Lisin on 02/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CacheItem : NSObject

@property (nonatomic) BOOL isRepost;
@property (nonatomic) BOOL isCompletelyLoaded;

@property (nonatomic) NSString *postID;
@property (nonatomic) NSString *date;

@property (nonatomic) NSString *senderAvatarLink;
@property (nonatomic) NSString *senderName;
@property (nonatomic) NSString *senderText;
@property (nonatomic) NSArray *senderPhotos;

@property (nonatomic) NSString *repostAvatarLink;
@property (nonatomic) NSString *repostName;
@property (nonatomic) NSString *repostText;
@property (nonatomic) NSArray *repostPhotos;

- (NSArray *) arrayWithItems;

@end
