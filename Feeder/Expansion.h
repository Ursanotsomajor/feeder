//
//  ExpansionClass.h
//  Created by Edward Lisin on 18/03/15.
//  Copyright (c) 2015 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import AVFoundation;


#define TIMEOUT 10; //Timeout for network requests

@interface Expansion: NSObject

#pragma mark – Threading
//Run block after delay
+ (void) runAfterDelay:(NSTimeInterval)delay inBlock:(void(^)())block;
//Run on the main thread
+ (void) runOnMainThread:(void (^)())block;
//Run on the background
+(void)runInBackground:(void(^)())block;

#pragma mark – Networking
+ (void) GETRequestWithString:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSDictionary *json, NSError *error))completion;
+ (void) POSTRequestWithString:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSDictionary *json, NSError *error))completion;
+ (void) GETDataRequest:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSData *data, NSError *error))completion;


#pragma mark – Other
//Debug
+ (void) debugMessage:(NSString *)message withError:(NSError *)error
             forClass:(id)currentClass methodName:(NSString *)methodName;
//Returns a view with a letter in the middle
+ (UIView *) avatarWithSize:(CGFloat)size backgroundColor:(UIColor *)backgroundColor initialLetter:(NSString *)initialLetter
                letterColor:(UIColor *)letterColor cornerRadius:(NSInteger)cornerRadius;

//Sound notifications
+ (AVAudioPlayer *) newNotificationWithType:(NSString *)type;
+ (void) vibrate;
//Alert
+ (void) alertWithTitle:(NSString *)title message:(NSString *)message
                okTitle:(NSString *)okTitle okAction:(void (^)())okAction
            cancelTitle:(NSString *)cancelTitle cancelAction:(void (^)())cancelAction;
//Decompress image
- (void) decompressImage:(UIImage*)image withCompletion:(void (^) (UIImage *))completion;
@end



@interface NSString (ExpansionString)
//Crop string from a to b
+ (NSString *) returnCroppedStringFromSource:(NSString *)sourceString byBeginning:(NSString *)beginningString andEnding:(NSString *)endingString;
//Capitalize first letter of a string
+ (NSString *) stringByCapitalizingFirstLetterOfString:(NSString *)string;
//Choose right form of a word @[@"День", @"Дня", @"Дней"]
+ (NSString *) rightFormFormForNumber:(NSString *)numberString formArray:(NSArray *)array;
- (NSString *) stringValue;
@end


@interface NSDictionary(ExpansionDictionary)
//Return dictionary with swapped value
+ (NSDictionary *) dictionaryByReplacingValueForKey:(NSString *)key withValue:(id)newValue inDictionary:(NSDictionary *)dictionary;
@end


@interface UIView (ExpansionView)
- (CGFloat) width;
- (CGFloat) height;

- (CGFloat) maxX;
- (CGFloat) x;

- (CGFloat) maxY;
- (CGFloat) y;

- (void) setWidth:(CGFloat) width;
- (void) setHeight:(CGFloat) height;
@end


@interface UIColor(ExpansionColor)
+ (UIColor *) colorFromHexString:(NSString *)hexString;
@end


@interface UIImage(ExpansionImage)
+ (UIImage *) imageWithView:(UIView *) view;
+ (UIImage *) imageWithColor:(UIColor *)color size:(CGSize)size;

@end

@interface NSURL(ExpansionURL)
+ (NSURL *) URLForObjectNamed:(NSString *)name;
@end
