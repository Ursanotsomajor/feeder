//
//  ImageTableViewCell.m
//  Feeder
//
//  Created by Edward Lisin on 29/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.contentView.alpha = 1;
        
    }];
}
 
@end
