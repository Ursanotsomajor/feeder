//
//  ExpansionClass.h
//  Created by Edward Lisin on 18/03/15.
//  Copyright (c) 2015 Edward Lisin. All rights reserved.
//

#import "Expansion.h"

@implementation Expansion

#pragma mark – Threading
+ (void) runAfterDelay:(NSTimeInterval)delay inBlock:(void (^)())block
{
    dispatch_time_t d_time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(d_time, dispatch_get_main_queue(), block);
}

+ (void) runOnMainThread:(void (^)())block
{
    dispatch_async(dispatch_get_main_queue(), block);
}

+(void)runInBackground:(void (^)())block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), block);
}

#pragma mark – Networking
+ (void) GETRequestWithString:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSDictionary *, NSError *))completion
{
    NSString *requestWithParameters = request;
    NSArray *parameterKeys = [parameters allKeys];
    
    if (parameters)
    {
        for (int i = 0; i < parameterKeys.count; i++)
        {
            if (i == 0)
            {
                requestWithParameters = [requestWithParameters stringByAppendingString:@"?"];
            }
            
            requestWithParameters = [requestWithParameters stringByAppendingString:[NSString stringWithFormat:@"%@=%@", parameterKeys[i], parameters[parameterKeys[i]]]];
            
            if (i == parameters.count - 1)
            {
                break;
            }
            
            requestWithParameters = [requestWithParameters stringByAppendingString:@"&"];
        }
    }
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    [[session dataTaskWithURL:[NSURL URLWithString:requestWithParameters] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          if (error)
          {
              [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(nil, error);}];
              return;
          }
          
          NSError *jsonError;
          NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
          
          if (jsonError)
          {
              completion(nil, jsonError);
              return;
          }
          
          [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(jsonDictionary, nil);}];
      }] resume];
}


+ (void)POSTRequestWithString:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSDictionary *, NSError *))completion
{
    NSString *parametersString = @"";
    NSArray *parameterKeys = [parameters allKeys];
    if (parameters)
    {
        for (int i = 0; i < parameterKeys.count; i++)
        {
            if (i == 0)
            {
                parametersString = [parametersString stringByAppendingString:@"?"];
            }
            
            parametersString = [parametersString stringByAppendingString:[NSString stringWithFormat:@"%@=%@", parameterKeys[i], parameters[parameterKeys[i]]]];
            
            if (i == parameters.count - 1)
            {
                break;
            }
            
            parametersString = [parametersString stringByAppendingString:@"&"];
        }
    }
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:request]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[parametersString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:nil
                                                            delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           if (error)
                                           {
                                               [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(nil, error);}];
                                               return;
                                           }
                                           
                                           NSError *jsonError;
                                           NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                                           
                                           if (jsonError)
                                           {
                                               completion(nil, jsonError);
                                               return;
                                           }
                                           
                                           [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(jsonDictionary, nil);}];
                                       }];
    
    [dataTask resume];
}


+ (void) GETDataRequest:(NSString *)request parameters:(NSDictionary *)parameters сompletion:(void (^)(NSData *, NSError *))completion
{
    NSString *requestWithParameters = request;
    NSArray *parameterKeys = [parameters allKeys];
    if (parameters)
    {
        for (int i = 0; i < parameterKeys.count; i++)
        {
            if (i == 0)
            {
                requestWithParameters = [requestWithParameters stringByAppendingString:@"?"];
            }
            
            requestWithParameters = [requestWithParameters stringByAppendingString:[NSString stringWithFormat:@"%@=%@", parameterKeys[i], parameters[parameterKeys[i]]]];
            
            if (i == parameters.count - 1)
            {
                break;
            }
            
            requestWithParameters = [requestWithParameters stringByAppendingString:@"&"];
        }
    }
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    [[session dataTaskWithURL:[NSURL URLWithString:requestWithParameters] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          if (error)
          {
              [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(nil, error);}];
              return;
          }
          
          [[NSOperationQueue mainQueue] addOperationWithBlock:^{completion(data, nil);}];
      }] resume];
}



#pragma mark – Other
+ (void) debugMessage:(NSString *)message withError:(NSError *)error
             forClass:(id)currentClass methodName:(NSString *)methodName
{
    NSString *messageText;
    if (error)
    {
        messageText = [NSString stringWithFormat:@"%@ -> %@: %@ – %@",
                       [currentClass class], methodName, message, error.localizedDescription];
    }
    else messageText = [NSString stringWithFormat:@"%@ -> %@: %@",
                        [currentClass class], methodName, message];
    
    
    NSString *separator = @"";
    
    for (int i = 0; i < messageText.length; i++)
    {
        separator = [separator stringByAppendingString:@"-"];
    }
    
    NSLog(@"\n%@\n%@\n%@", separator, messageText, separator);
}

+ (UIView *) avatarWithSize:(CGFloat)size backgroundColor:(UIColor *)backgroundColor initialLetter:(NSString *)initialLetter
                letterColor:(UIColor *)letterColor cornerRadius:(NSInteger)cornerRadius
{
    CGRect  viewRect = CGRectMake(0, 0, size, size);
    
    UIView* myView = [[UIView alloc] initWithFrame:viewRect];
    myView.layer.cornerRadius = cornerRadius;
    
    myView.backgroundColor = backgroundColor;
    
    UILabel *letterLabel = [[UILabel alloc] initWithFrame:myView.frame];
    letterLabel.textAlignment = NSTextAlignmentCenter;
    letterLabel.font = [UIFont fontWithName:@"Helvetica" size:size/1.8];
    letterLabel.text = initialLetter;
    letterLabel.textColor = letterColor;
    [myView addSubview:letterLabel];
    
    return myView;
}

+ (AVAudioPlayer *) newNotificationWithType:(NSString *)type
{
    
    if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"Settings"] objectForKey:@"Sound"] boolValue])
    {
        NSString *fileName;
        
        if ([type isEqualToString:@"Send"])
        {
            fileName = @"Send";
        }
        
        else if ([type isEqualToString:@"Incoming"])
        {
            fileName = @"Incoming";
        }
        
        else return nil;
        
        NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
        NSError *err;
        
        AVAudioPlayer *theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&err];
        theAudio.volume = [[AVAudioSession sharedInstance] outputVolume];
        
        return theAudio;
    }
    
    else return nil;
}

+ (void) vibrate
{
    if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"Settings"] objectForKey:@"Vibration"] boolValue])
    {
        AudioServicesPlaySystemSound(4095);
    }
}

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message
               okTitle:(NSString *)okTitle okAction:(void (^)())okAction
           cancelTitle:(NSString *)cancelTitle cancelAction:(void (^)())cancelAction
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:okTitle ? okTitle : @"Ок" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action){
                                                   
                                                   if (okAction)
                                                   {
                                                       okAction();
                                                   }
                                                   
                                               }];
    [alert addAction:ok];
    
    if (cancelTitle)
    {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelTitle ? cancelTitle : @"Отменить"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action){
                                                           
                                                           if (cancelAction)
                                                           {
                                                               cancelAction();
                                                           }
                                                           
                                                       }];
        
        [alert addAction:cancel];
    }
    
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void) decompressImage:(UIImage*)image withCompletion:(void (^) (UIImage *))completion
{
    UIGraphicsBeginImageContext(image.size);
    [image drawAtPoint:CGPointZero];
    UIImage *decompressedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    completion(decompressedImage);
}
@end



@implementation NSString (ExpansionString)
+ (NSString *) returnCroppedStringFromSource:(NSString *)sourceString byBeginning:(NSString *)beginningString andEnding:(NSString *)endingString
{
    NSRange firstRange = [sourceString rangeOfString:beginningString];
    
    if (firstRange.location == NSNotFound) {return nil;}
    
    NSInteger integer = firstRange.location + firstRange.length;
    sourceString = [sourceString substringFromIndex:integer];
    
    NSRange secondRange = [sourceString rangeOfString:endingString];
    
    return [sourceString substringToIndex:secondRange.location];
}

+ (NSString *) stringByCapitalizingFirstLetterOfString:(NSString *)string;
{
    return [string stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[string substringToIndex:1] capitalizedString]];
}

+ (NSString *) rightFormFormForNumber:(NSString *)numberString formArray:(NSArray *)array
{
    switch ([[numberString substringFromIndex:numberString.length - 1] integerValue])
    {
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            return array[2];
            break;
            
        case 1:
            return array[0];
            break;
            
        case 2:
        case 3:
        case 4:
            return array[1];
            break;
            
        default:
            return nil;
            break;
    }
}

- (NSString *)stringValue
{
    return self;
}
@end



@implementation NSDictionary (ExpansionDictionary)
+ (NSDictionary *) dictionaryByReplacingValueForKey:(NSString *)key withValue:(id)newValue inDictionary:(NSDictionary *) oldDictionary
{
    NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
    newDictionary = [oldDictionary mutableCopy];
    
    [newDictionary removeObjectForKey:key];
    [newDictionary setObject:newValue forKey:key];
    
    return newDictionary;
}
@end



@implementation UIView (ExpansionView)
-(CGFloat) width
{
    return CGRectGetWidth(self.bounds);
}

-(CGFloat) height
{
    return CGRectGetHeight(self.bounds);
}

- (CGFloat) maxX
{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat) x
{
    return self.frame.origin.x;
}

- (CGFloat) maxY
{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat) y
{
    return self.frame.origin.y;
}

-(void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
@end



@implementation UIColor (ExpansionColor)
+ (UIColor *) colorFromHexString:(NSString *)hexString
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3)
    {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
@end

@implementation UIImage (ExpansionImage)
+ (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *) imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width, size.height), YES, 0);
    [color setFill];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end


@implementation NSURL(ExpansionURL)
+ (NSURL *) URLForObjectNamed:(NSString *)name
{
    return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], name]];
}
@end
