//
//  CachedItem+CoreDataProperties.h
//  Feeder
//
//  Created by Edward Lisin on 17/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CachedItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface CachedItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSNumber *isRepost;
@property (nullable, nonatomic, retain) NSString *postID;
@property (nullable, nonatomic, retain) NSString *repostAvatarLink;
@property (nullable, nonatomic, retain) NSString *repostName;
@property (nullable, nonatomic, retain) NSData *repostPhotos;
@property (nullable, nonatomic, retain) NSString *repostText;
@property (nullable, nonatomic, retain) NSString *senderAvatarLink;
@property (nullable, nonatomic, retain) NSString *senderName;
@property (nullable, nonatomic, retain) NSData *senderPhotos;
@property (nullable, nonatomic, retain) NSString *senderText;

@end

NS_ASSUME_NONNULL_END
