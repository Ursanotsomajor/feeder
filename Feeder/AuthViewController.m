//
//  AuthViewController.m
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "AuthViewController.h"
#import "AppDelegate.h"
#import <VKSdk.h>

@interface AuthViewController () <VKSdkDelegate, VKSdkUIDelegate>

@property VKSdk *sdkInstance;
@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet UILabel *subheaderLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end


@implementation AuthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _sdkInstance = [VKSdk initializeWithAppId:VK_APP_ID];
    [_sdkInstance registerDelegate:self];
    [_sdkInstance setUiDelegate:self];
    
    [VKSdk wakeUpSession:@[@"Wall", @"Friends"] completeBlock:^(VKAuthorizationState state, NSError *error) {
        
    }];
    
    if ([VKSdk isLoggedIn])
    {
        [self proceedToFeed];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    _logoLabel.alpha = 0;
    _subheaderLabel.alpha = 0;
    _loginButton.alpha = 0;
    
    [UIView animateWithDuration:1 animations:^{
        
        _logoLabel.alpha = 1;
        _subheaderLabel.alpha = 1;
        _loginButton.alpha = 1;
        
    }];
}


- (IBAction)enterPressed:(id)sender
{
    [VKSdk authorize:@[@"Wall", @"Friends"]];
}

- (void) proceedToFeed
{
    [self performSegueWithIdentifier:@"ProceedToFeed" sender:self];
}


#pragma mark – VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if (!result.error)
    {
        [[[VKApi users] get:@{VK_API_FIELDS : @"first_name, last_name" }] executeWithResultBlock:^(VKResponse *response) {
            
            VKUser * user = [(VKUsersArray*)[response parsedModel] firstObject];
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@", user.first_name, user.last_name] forKey:@"Username"];
            
            [self proceedToFeed];
            
        } errorBlock:^(NSError *error) {
            
            NSLog(@"Error: %@", error);
            
        }];
    }
    
    else
    {
        NSLog(@"%@", result.error);
    }
}


- (void)vkSdkUserAuthorizationFailed
{
    
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

@end
