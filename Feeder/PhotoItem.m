//
//  PhotoItem.m
//  Feeder
//
//  Created by Edward Lisin on 29/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "PhotoItem.h"

@implementation PhotoItem

+ (instancetype)photoItemWithDictionary:(NSDictionary *)dictionary
{
    PhotoItem *photoItem = [PhotoItem new];
    
    photoItem.imageLink = dictionary[@"photo"][@"photo_604"];

    photoItem.imageWidth = [dictionary[@"photo"][@"width"] floatValue];
    photoItem.imageHeight = [dictionary[@"photo"][@"height"] floatValue];
    
    return photoItem;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{

    [encoder encodeObject:self.imageLink forKey:@"ImageLink"];
    [encoder encodeObject:@(self.imageWidth) forKey:@"ImageWidth"];
    [encoder encodeObject:@(self.imageHeight) forKey:@"ImageHeight"];
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.imageLink = [decoder decodeObjectForKey:@"ImageLink"];
        self.imageWidth = [[decoder decodeObjectForKey:@"ImageWidth"] floatValue];
        self.imageHeight = [[decoder decodeObjectForKey:@"ImageHeight"] floatValue];
    }
    
    return self;
}

@end
