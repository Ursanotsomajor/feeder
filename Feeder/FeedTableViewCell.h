//
//  FeedTableViewCell.h
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Expansion.h"
#import "CacheItem.h"

@interface FeedTableViewCell : UITableViewCell

@property (nonatomic) CacheItem *cacheItem;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *repostAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIImageView *repostImageView;

@property (nonatomic) UIImage *postImage;
@property (nonatomic) UIImage *repostImage;
@property (nonatomic) UIImage *avatarImage;
@property (nonatomic) UIImage *repostAvatarImage;

@end
