//
//  PhotoItem.h
//  Feeder
//
//  Created by Edward Lisin on 29/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoItem : NSObject

@property (nonatomic) NSString *imageLink;

@property (nonatomic) float imageWidth;
@property (nonatomic) float imageHeight;

+ (instancetype) photoItemWithDictionary:(NSDictionary *)dictionary;
- (void)encodeWithCoder:(NSCoder *)encoder;

@end
