//
//  RepostTableViewCell.h
//  Feeder
//
//  Created by Edward Lisin on 10/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepostTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
