//
//  DataSource.m
//  Feeder
//
//  Created by Edward Lisin on 03/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import "DataSource.h"


@interface DataSource()

@property (nonatomic) NSCache *feedCache;

@end


@implementation DataSource

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _feedCache = [[NSCache alloc] init];
        _feedCache.countLimit = 100;
        
        _coreDataController = [[CoreDataController alloc] init];
    }
    
    return self;
}


- (void) getImageForLink:(NSString *)link withCompletion:(void (^)(UIImage *))completion
{
    NSString *shortLink = [CoreDataController shortAdressFromLink:link];
    if ([_coreDataController hasImageForLink:shortLink])
    {
        completion([UIImage imageWithData:[_coreDataController imageDataForLink:shortLink]]);
        return;
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:link] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error)
        {
            return;
        }
        
        if (data)
        {
            UIImage *image = [UIImage imageWithData:data];
            
            if (image)
            {
                completion(image);
                [_coreDataController insertPhotoWithLink:link imageData:data];
            }
        }
        
    }] resume];
}


- (void)cacheItemWithDictionary:(NSDictionary *)dictionary completion:(void (^)(CacheItem *))completion
{
    if ([_feedCache objectForKey:dictionary[@"post_id"]])
    {
        completion([_feedCache objectForKey:dictionary[@"post_id"]]);
        return;
    }

    
    CacheItem *cachedItem = [CacheItem new];
    
    //Date –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dictionary[@"date"] doubleValue]];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    cachedItem.date = [formatter stringFromDate:date];
    
    
    //Sender text, Post ID –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    cachedItem.senderText = dictionary[@"text"];
    cachedItem.postID = dictionary[@"post_id"];
    
    //Sender attachments –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    NSMutableArray *photos = [NSMutableArray new];
    for (NSDictionary *attachment in dictionary[@"attachments"])
    {
        if ([attachment[@"type"] isEqualToString:@"link"])
        {
            NSString *link = attachment[@"link"][@"url"];
            if (cachedItem.senderText && cachedItem.senderText.length > 0)
            {
                cachedItem.senderText = [cachedItem.senderText
                                         stringByAppendingString:[NSString stringWithFormat:@"\n\n %@", link]];
            }
            else cachedItem.senderText = link;
        }
        
        if ([attachment[@"type"] isEqualToString:@"photo"])
        {
            PhotoItem *photo = [PhotoItem photoItemWithDictionary:attachment];
            [photos addObject:photo];
        }
    }
    cachedItem.senderPhotos = [photos copy];
    
    
    //User ID, Avatar, Name –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    NSString *userid = [NSString stringWithFormat:@"%@", dictionary[@"source_id"]];
    if ([userid containsString:@"-"])
    {
        userid = [userid substringFromIndex:1];
        
        [VKHelper getGroupInfoForID:userid completion:^(NSDictionary *groupItem) {
            
                cachedItem.senderAvatarLink = groupItem[@"photo_100"];
                cachedItem.senderName = groupItem[@"name"];
                
                if ([self checkCompletion:cachedItem])
                {
                    completion(cachedItem);
                    return;
                }
            
        }];
    }
    else
    {
        [VKHelper getUserInfoForID:userid completion:^(NSDictionary *userItem) {
            
                cachedItem.senderAvatarLink = userItem[@"photo_100"];
                cachedItem.senderName = [NSString stringWithFormat:@"%@ %@", userItem[@"first_name" ], userItem[@"last_name"]];
                
                if ([self checkCompletion:cachedItem])
                {
                    completion(cachedItem);
                    return;
                }
            
        }];
    }
    
    
    //If there is repost data ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    if (dictionary[@"copy_history"])
    {
        cachedItem.isRepost = 1;
        cachedItem.repostText = dictionary[@"copy_history"][0][@"text"];
        
        //Repost photos
        NSMutableArray *photos = [NSMutableArray new];
        for (NSDictionary *attachment in dictionary[@"copy_history"][0][@"attachments"])
        {
            if ([attachment[@"type"] isEqualToString:@"link"])
            {
                NSString *link = attachment[@"link"][@"url"];
                if (cachedItem.repostText && cachedItem.repostText.length > 0)
                {
                    cachedItem.repostText = [cachedItem.repostText
                                             stringByAppendingString:[NSString stringWithFormat:@"\n\n %@", link]];
                }
                else cachedItem.repostText = link;
            }
            
            if ([attachment[@"type"] isEqualToString:@"photo"])
            {
                PhotoItem *photo = [PhotoItem photoItemWithDictionary:attachment];
                [photos addObject:photo];
            }
        }
        cachedItem.repostPhotos = [photos copy];
        
        
        //Repost ID
        NSString *repostID = [NSString stringWithFormat:@"%@", dictionary[@"copy_history"][0][@"from_id"]];
        if ([repostID containsString:@"-"])
        {
            repostID = [repostID substringFromIndex:1];
            
            [VKHelper getGroupInfoForID:repostID completion:^(NSDictionary *groupItem) {
                
                    cachedItem.repostAvatarLink = groupItem[@"photo_100"];
                    cachedItem.repostName = groupItem[@"name"];
                    
                    if ([self checkCompletion:cachedItem])
                    {
                        completion(cachedItem);
                        return;
                    }
                
            }];
        }
        else
        {
            [VKHelper getUserInfoForID:repostID completion:^(NSDictionary *userItem) {
                
                    cachedItem.repostAvatarLink = userItem[@"photo_100"];
                    cachedItem.repostName = [NSString stringWithFormat:@"%@ %@", userItem[@"first_name"], userItem[@"last_name"]];
                    
                    if ([self checkCompletion:cachedItem])
                    {
                        completion(cachedItem);
                        return;
                    }
                
            }];
        }
    }
}

- (BOOL) checkCompletion:(CacheItem *)cachedItem
{
    if (!cachedItem.senderAvatarLink)
    {
        return NO;
    }
    
    if (cachedItem.isRepost && !cachedItem.repostAvatarLink)
    {
        return NO;
    }
    
    [_feedCache setObject:cachedItem forKey:cachedItem.postID];
    
    return YES;
}

- (CGFloat)heightForCacheItemWithDictionary:(NSDictionary *)dictionary width:(CGFloat)width
{
    CGFloat height = 70;
    
    //Text
    if (![dictionary[@"text"] isEqualToString:@""])
    {
        height += [self heightForText:dictionary[@"text"] withWidth:width] + 30;
    }
    
    //Image
    NSMutableArray *photos = [NSMutableArray new];
    for (NSDictionary *attachment in dictionary[@"attachments"])
    {
        if ([attachment[@"type"] isEqualToString:@"link"])
        {
            NSString *link = attachment[@"link"][@"url"];
            if (![dictionary[@"text"] isEqualToString:@""])
            {
                height += [self heightForText:[NSString stringWithFormat:@"\n\n %@", link] withWidth:width];
            }
            else height = [self heightForText:link withWidth:width] + 20;
        }
        
        if ([attachment[@"type"] isEqualToString:@"photo"])
        {
            PhotoItem *photo = [PhotoItem photoItemWithDictionary:attachment];
            [photos addObject:photo];
            
            height += (width * [photo imageHeight])/[photo imageWidth];
            break;
        }
    }
    
    
    if (dictionary[@"copy_history"])
    {
        height += 50;
        
        
        if (![dictionary[@"copy_history"][0][@"text"] isEqualToString:@""])
        {
            height += [self heightForText:dictionary[@"copy_history"][0][@"text"] withWidth:width] + 30;
        }
        
        NSMutableArray *repostPhotos = [NSMutableArray new];
        for (NSDictionary *attachment in dictionary[@"copy_history"][0][@"attachments"])
        {
            if ([attachment[@"type"] isEqualToString:@"link"])
            {
                NSString *link = attachment[@"link"][@"url"];
                if (![dictionary[@"copy_history"][0][@"text"] isEqualToString:@""])
                {
                    height += [self heightForText:[NSString stringWithFormat:@"\n\n %@", link] withWidth:width];
                }
                else height = [self heightForText:link withWidth:width] + 20;
            }
            
            if ([attachment[@"type"] isEqualToString:@"photo"])
            {
                PhotoItem *photo = [PhotoItem photoItemWithDictionary:attachment];
                [repostPhotos addObject:photo];
                
                height += (width * [photo imageHeight])/[photo imageWidth];
                break;
            }
        }
        
        if (photos.count > 1 || repostPhotos.count > 1)
            height += 30;
    }
    else
    {
        if (photos.count > 1)
        {
            height += 30;
        }
    }
    
    return height;
}

- (CGFloat) heightForText:(NSString *)text withWidth:(CGFloat)width
{
    return [text boundingRectWithSize:CGSizeMake(width - 16, 600)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]}
                               context:nil].size.height;
}

@end
