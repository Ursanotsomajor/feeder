//
//  Photo+CoreDataProperties.m
//  Feeder
//
//  Created by Edward Lisin on 17/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Photo+CoreDataProperties.h"

@implementation Photo (CoreDataProperties)

@dynamic imageData;
@dynamic url;
@dynamic createdAt;

@end
