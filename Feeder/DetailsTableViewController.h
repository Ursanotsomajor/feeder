//
//  DetailsTableViewController.h
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CacheItem.h"
#import "DataSource.h"

@interface DetailsTableViewController : UITableViewController

@property (nonatomic) DataSource *dataSource;
@property (nonatomic) NSDictionary *feedItem;

@end
