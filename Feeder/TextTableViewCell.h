//
//  TextTableViewCell.h
//  Feeder
//
//  Created by Edward Lisin on 29/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *postTextView;

@end
