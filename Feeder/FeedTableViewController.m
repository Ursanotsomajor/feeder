//
//  FeedTableViewController.m
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "FeedTableViewController.h"
#import "VKHelper.h"
#import "FeedTableViewCell.h"
#import "Expansion.h"
#import "DetailsTableViewController.h"
#import "CacheItem.h"
#import "DataSource.h"


@interface FeedTableViewController ()

@property DataSource *dataSource;
@property NSMutableArray *feedItems;
@property NSString *nextFrom;

@end


@implementation FeedTableViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:NO];
    self.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"Username"];
    
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"logoutBarButtonImage"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                     style:UIBarButtonItemStylePlain target:self action:@selector(logoutPressed)];
    [self.navigationItem setLeftBarButtonItem:logoutButton];
    
    
    _dataSource = [[DataSource alloc] init];
    _feedItems = [NSMutableArray new];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshPosts) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    
    [self loadNext];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [SVProgressHUD dismiss];
}

#pragma mark – Getting Information

- (void) refreshPosts
{
    _nextFrom = @"";
    _feedItems = [NSMutableArray new];
    
    [self loadNext];
}

- (void) loadNext
{
    [SVProgressHUD show];
    
    [VKHelper getFeedWithOffset:_nextFrom ? _nextFrom : @"" Completion:^(NSDictionary *feedItem, VKHelperError error) {
        
        [SVProgressHUD dismiss];
        [self.refreshControl endRefreshing];
        
        if (feedItem)
        {
            [_feedItems addObjectsFromArray:feedItem[@"items"]];
            _nextFrom = feedItem[@"next_from"];
            
            [self.tableView reloadData];
        }
        
        else
        {
            [self messageForError:error];
        }
        
    }];
}

#pragma mark – Other

- (void) logoutPressed
{
    [Expansion alertWithTitle:@"Хотите выйти или случайно нажали?" message:nil
                      okTitle:@"Выйти" okAction:^{
                          
                          [self logout];
                          
                      } cancelTitle:@"Случайно" cancelAction:nil];
}

- (void) logout
{
    [VKHelper logout];
    [_dataSource.coreDataController removeAllEntities];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)messageForError:(VKHelperError)error
{
    if (error == VKHelperTokenError)
    {
        [Expansion alertWithTitle:@"Ошибка" message:@"Отсутствует токен"
                          okTitle:nil okAction:^{
                          
                              [self logout];
                              
                          } cancelTitle:nil cancelAction:nil];
    }
    
    if (error == VKHelperConnectionError)
    {
        [Expansion alertWithTitle:nil message:@"Нет подключения к Интернету"
                          okTitle:nil okAction:nil cancelTitle:nil cancelAction:nil];
    }
    
    if (error == VKHelperTokenUnknownError)
    {
        [Expansion alertWithTitle:@"Неизвестная ошибка" message:nil
                          okTitle:nil okAction:nil cancelTitle:nil cancelAction:nil];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _feedItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [_dataSource heightForCacheItemWithDictionary:_feedItems[indexPath.row] width:self.tableView.width];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _feedItems.count - 1)
    {
        [self loadNext];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.avatarImageView.image = nil;
    cell.repostAvatarImageView.image = nil;
    cell.postImageView.image = nil;
    cell.repostImageView.image = nil;
    cell.cacheItem = nil;
    
    [_dataSource cacheItemWithDictionary:_feedItems[indexPath.row] completion:^(CacheItem *cacheItem) {
        
        [cell setCacheItem:cacheItem];
        
        if (cacheItem.senderPhotos.count)
        {
            [_dataSource getImageForLink:[cacheItem.senderPhotos[0] imageLink] withCompletion:^(UIImage *image) {
                cell.postImage = image;
            }];
        }
        
        [_dataSource getImageForLink:cacheItem.senderAvatarLink withCompletion:^(UIImage *image) {
            cell.avatarImage = image;
        }];
        
        
        if (cacheItem.isRepost)
        {
            if (cacheItem.repostPhotos.count)
            {
                [_dataSource getImageForLink:[cacheItem.repostPhotos[0] imageLink] withCompletion:^(UIImage *image) {
                    cell.repostImage = image;
                }];
            }
            
            [_dataSource getImageForLink:cacheItem.repostAvatarLink withCompletion:^(UIImage *image) {
                cell.repostAvatarImage = image;
            }];
        }
        
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailsTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsTableViewController"];
    
    //Спросить насчет этого
    vc.dataSource = _dataSource;
    vc.feedItem = _feedItems[indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
