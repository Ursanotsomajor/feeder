//
//  CachedItem+CoreDataProperties.m
//  Feeder
//
//  Created by Edward Lisin on 17/05/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CachedItem+CoreDataProperties.h"

@implementation CachedItem (CoreDataProperties)

@dynamic date;
@dynamic isRepost;
@dynamic postID;
@dynamic repostAvatarLink;
@dynamic repostName;
@dynamic repostPhotos;
@dynamic repostText;
@dynamic senderAvatarLink;
@dynamic senderName;
@dynamic senderPhotos;
@dynamic senderText;

@end
