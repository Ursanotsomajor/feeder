//
//  DetailsTableViewController.m
//  Feeder
//
//  Created by Edward Lisin on 16/04/16.
//  Copyright © 2016 Edward Lisin. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "DetailsTableViewController.h"
#import "UserInfoTableViewCell.h"
#import "TextTableViewCell.h"
#import "ImageTableViewCell.h"
#import "RepostTableViewCell.h"
#import "Expansion.h"


@interface DetailsTableViewController ()

@property (nonatomic) NSArray *items;

@end


@implementation DetailsTableViewController

- (void)viewWillDisappear:(BOOL)animated
{
    [SVProgressHUD dismiss];
}

- (void)setFeedItem:(NSDictionary *)feedItem
{
    _feedItem = feedItem;
    
    [SVProgressHUD show];
    [_dataSource cacheItemWithDictionary:feedItem completion:^(CacheItem *cacheItem) {
        
        [SVProgressHUD dismiss];
        
        _items = [cacheItem arrayWithItems];
        [self.tableView reloadData];
        
    }];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_items[indexPath.row] isKindOfClass:[NSDictionary class]])
    {
        if (indexPath.row == 0)
        {
            return 70;
        }
        
        else
        {
            return 50;
        }
    }
    
    if ([_items[indexPath.row] isKindOfClass:[PhotoItem class]])
    {
        CGFloat height = (self.view.bounds.size.width * [_items[indexPath.row] imageHeight])/[_items[indexPath.row] imageWidth];
        
        return height;
    }
    
    if ([_items[indexPath.row] isKindOfClass:[NSString class]])
    {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16]};
        CGRect rect = [_items[indexPath.row] boundingRectWithSize:CGSizeMake(self.view.bounds.size.width, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:attributes
                                                          context:nil];
        return rect.size.height + 20;
    }
    
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_items[indexPath.row] isKindOfClass:[NSDictionary class]])
    {
        if (indexPath.row == 0)
        {
            UserInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
            
            [Expansion runOnMainThread:^{
                
                cell.avatarImageView.image = nil;
                [_dataSource getImageForLink:_items[indexPath.row][@"Avatar"] withCompletion:^(UIImage *image) {
                    cell.avatarImageView.image = image;
                }];
                
                cell.dateLabel.text = _items[indexPath.row][@"Date"];
                cell.nameLabel.text = _items[indexPath.row][@"Name"];
                
            }];
            
            return cell;
        }
        
        else
        {
            RepostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepostCell"];
            
            [Expansion runOnMainThread:^{
                
                cell.avatarImageView.image = nil;
                [_dataSource getImageForLink:_items[indexPath.row][@"Avatar"] withCompletion:^(UIImage *image) {
                   cell.avatarImageView.image = image;
                }];
                
                cell.nameLabel.text = _items[indexPath.row][@"Name"];
                
            }];
            
            return cell;
        }
    }
    
    if ([_items[indexPath.row] isKindOfClass:[PhotoItem class]])
    {
        ImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
        
        cell.photoImageView.image = nil;
        [_dataSource getImageForLink:[_items[indexPath.row] imageLink] withCompletion:^(UIImage *image) {
            
            [Expansion runOnMainThread:^{
                cell.photoImageView.image = image;
            }];
            
        }];
        
        return cell;
    }
    
    if ([_items[indexPath.row] isKindOfClass:[NSString class]])
    {
        TextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextCell"];
        
        [Expansion runOnMainThread:^{
            cell.postTextView.text = _items[indexPath.row];
        }];
        
        return cell;
    }
    
    return nil;
}

@end
